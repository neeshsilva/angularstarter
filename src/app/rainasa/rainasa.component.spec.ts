import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RainasaComponent } from './rainasa.component';

describe('RainasaComponent', () => {
  let component: RainasaComponent;
  let fixture: ComponentFixture<RainasaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RainasaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RainasaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
