import { Component, OnInit, Input } from '@angular/core';
import { Rainasa } from '../rainasa';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  @Input('footerInfo') footerInfo :Rainasa;

  constructor() { }

  ngOnInit() {
  }

}
